#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char **argv){
	time_t sec;
	pid_t pid;
	long child_pid, parent_pid;
	int status;

	sec = time(NULL);
	printf("%ld\n", sec);
	
	sec = time(NULL);
	printf("%ld\n", sec);
	
	//Create a child process using fork
	pid = fork();
	if(pid == 0){
		printf("I am the child!!\n");
		child_pid = (long)getpid();
		printf("I am done as a child!\n");
		exit(2);
	} else if(pid != -1) {
		printf("I am the parent!!\n");
		while(!WIFEXITED(status)){
			waitpid(-child_pid, &status, WCONTINUED);
			if(WIFEXITED(status)){
				printf("exited");
				parent_pid = (long)getpid();
				printf("The child is finished with a pid of: %ld\n",(long)child_pid);	
			}
		}
	} 


}
